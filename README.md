# A03 

personal website

## How to use

navigate to ./a03Gurudu folder.

Run npm install to install all the dependencies in the package.json file.
```
> npm install
```

Run node app.js to start the server.  (Hit CTRL-C to stop.)

```
> node gbapp.js
```

Point your browser to `http://localhost:8081`. 
